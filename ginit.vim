" let $NVIM_TUI_ENABLE_TRUE_COLOR=1
set termguicolors
set background=dark
GuiFont JetBrainsMono\ Nerd\ Font:b:h9
GuiTabline 0
GuiPopupmenu 0

let s:fontsize = 12
function! AdjustFontSize(amount)
  let s:fontsize = s:fontsize+a:amount
  :execute "GuiFont! JetBrainsMono\ Nerd\ Font:b:h" . s:fontsize
endfunction

noremap <C-ScrollWheelUp> :call AdjustFontSize(1)<CR>
noremap <C-ScrollWheelDown> :call AdjustFontSize(-1)<CR>
inoremap <C-ScrollWheelUp> <Esc>:call AdjustFontSize(1)<CR>a
inoremap <C-ScrollWheelDown> <Esc>:call AdjustFontSize(-1)<CR>a
