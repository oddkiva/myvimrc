" ==============================================================================
" => Keybindings
"
" Set the leader key
let mapleader=";"
"
" => Moving around, tabs, windows and buffers
"
" Treat long lines as break lines (useful when moving around in them)
map j gj
map k gk

" Disable highlight when <leader><CR> is pressed.
map <silent> <leader><CR> :noh<CR>

" Close all the buffers.
map <leader>ba :%bd!<CR>

" Useful mappings for managing tabs
map <leader>tn :tabnew<CR>
map <leader>to :tabonly<CR>
map <leader>tc :tabclose<CR>
map <leader>tm :tabmove

" Switch CWD to the directory of the open buffer
map <leader>cd :cd %:p:h<CR>:pwd<CR>

" Close split buffer without killing split.
map <leader>tq :bp\|bd #<CR>

" Visual mode pressing * or # searches for the current selection
" Super useful! From an idea by Michael Naumann
vnoremap <silent> * :call VisualSelection('f')<CR>
vnoremap <silent> # :call VisualSelection('b')<CR>

" Plugin specific options.
map <S-Tab> :NvimTreeToggle<CR>
map <C-p> :Files<CR>

" Instantiate a terminal buffer.
:nnoremap <leader>t :wincmd b \| bel terminal<CR>
" When using the terminal, pressing the Escape key will make us go back to
" visual mode on the terminal.
autocmd! TermOpen * tnoremap <ESC> <C-\><C-n>

" When using FZF plugin, pressing the Escape key will make us go back to
" visual mode on the terminal.
autocmd! FileType fzf tnoremap <buffer> <ESC> <C-C>

" Autocomplete filesystem path with FZF!
imap <C-x><C-f> <plug>(fzf-complete-path)

" => Preview images with Kitty.
" autocmd BufEnter *.png,*.jpg,*gif,*.tif exec "!kitty +kitten icat ".expand("%") | :bw
