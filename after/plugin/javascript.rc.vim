" => Javascript IDE.
"
autocmd FileType svelte,html,javascript,typescript noremap <C-K><C-F> :PrettierAsync<CR>

let g:javascript_plugin_flow = 1
let g:jsx_ext_required = 0
let g:ale_lint_on_save = 1
let g:ale_lint_on_text_changed = 0

" Prettier Settings
let g:prettier#quickfix_enabled = 0
let g:prettier#autoformat_require_pragma = 0
