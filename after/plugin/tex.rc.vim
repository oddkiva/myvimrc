" => LaTeX IDE.
"
" Prerequisites:
" - brew install basictex
" - brew tap zegervdv/zathura
" - brew install zathura
" - sudo tlmgr install latexmk
set conceallevel=2
let g:tex_conceal="abdgm"

" This is necessary for VimTeX to load properly. The "indent" is optional.
" Note that most plugin managers will do this automatically.
filetype plugin indent on

" This enables Vim's and neovim's syntax-related features. Without this, some
" VimTeX features will not work (see ":help vimtex-requirements" for more
" info).
syntax enable

if has('macunix')
  " Viewer options: One may configure the viewer either by specifying a built-in
  " viewer method:
  let g:vimtex_view_method = 'zathura'
elseif has('unix')
  " Or with a generic interface:
  let g:vimtex_view_general_viewer = 'zathura'
endif
let g:vimtex_view_general_options = '--unique file:@pdf\#src:@line@tex'

" Most VimTeX mappings rely on localleader and this can be changed with the
" following line. The default is usually fine and is the symbol "\".
let maplocalleader = ","


" if has('macunix')
"   let g:livepreview_previewer = 'zathura'
" else
"   let g:livepreview_previewer = 'atril'
" endif
" 
" autocmd FileType tex setlocal updatetime=1
" autocmd FileType tex map <C-F12> :LLPStartPreview<CR>
" " Reformat lines (getting the spacing correct) {{{
" fun! TeX_fmt()
"   if (getline(".") != "")
"     let save_cursor = getpos(".")
"     let op_wrapscan = &wrapscan
"     set nowrapscan
"     let par_begin = '^\(%D\)\=\s*\($\|\\start\|\\stop\|\\Start\|\\Stop\|\\\(sub\)*section\>\|\\item\>\|\\NC\>\|\\blank\>\|\\noindent\>\)'
"     let par_end   = '^\(%D\)\=\s*\($\|\\start\|\\stop\|\\Start\|\\Stop\|\\place\|\\\(sub\)*section\>\|\\item\>\|\\NC\>\|\\blank\>\)'
"     try
"       exe '?'.par_begin.'?+'
"     catch /E384/
"       1
"     endtry
"     norm V
"     try
"       exe '/'.par_end.'/-'
"     catch /E385/
"       $
"     endtry
"     norm gq
"     let &wrapscan = op_wrapscan
"     call setpos('.', save_cursor)
"   endif
" endfun
" autocmd FileType tex nmap Q :call TeX_fmt()<CR>
