" Search for the word under the cursor.
autocmd FileType c,cpp,objc nnoremap K :grep! "\b<C-R><C-W>\b"<CR>:cw<CR>

" Shortcut key for code autoformatting.
autocmd FileType c,cpp,objc noremap <C-K><C-F> :Autoformat<CR>

" Editing options.
autocmd FileType c,cpp,objc setlocal cindent
autocmd FileType c,cpp,objc setlocal cino=(0,W4,g0,i-s,:0
autocmd FileType c,cpp,objc setlocal foldmethod=syntax

" Use clang-format in C-family based code.
let s:configfile_def = "'clang-format
      \ -lines='.a:firstline.':'.a:lastline.'
      \ --assume-filename='.bufname('%').'
      \ -style=file'"
let s:noconfigfile_def = "'clang-format
      \ -lines='.a:firstline.':'.a:lastline.'
      \ --assume-filename='.bufname('%').'
      \ -style=\"{
      \   BasedOnStyle: WebKit,
      \   AlignTrailingComments: true,
      \   '.(&textwidth ? 'ColumnLimit: '.&textwidth.', ' : '').(&expandtab ? 'UseTab: Never, IndentWidth: '.shiftwidth() : 'UseTab: Always').'
      \ }\"'"
let g:formatdef_clangformat = "g:ClangFormatConfigFileExists()
      \ ? (" . s:configfile_def . ")
      \ : (" . s:noconfigfile_def . ")"

" Debugger
packadd termdebug
autocmd FileType c,cpp nnoremap <Leader>b :Break<CR>
autocmd FileType c,cpp nnoremap <Leader>d :Delete<CR>
autocmd FileType c,cpp nnoremap <Leader>s :Step<CR>
autocmd FileType c,cpp nnoremap <Leader>n :Next<CR>
autocmd FileType c,cpp nnoremap <Leader>o :Over<CR>
autocmd FileType c,cpp nnoremap <Leader>c :Continue<CR>
hi debugPC term=reverse ctermbg=darkblue guibg=darkblue
hi debugBreakpoint term=reverse ctermbg=red guibg=red
