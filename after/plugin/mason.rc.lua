require("mason").setup{}
local mason_lspconfig = require("mason-lspconfig")

mason_lspconfig.setup {
  ensure_installed = {
    "clangd",
    "cmake",
    "vimls",
    "lua_ls",
    "svelte",
    "tailwindcss",
    "ts_ls",
    "marksman"
  }
}
