local lspconfig = require("lspconfig")

-- Setup the following language servers.
lspconfig.clangd.setup {
  cmd = {
    "clangd",
    "--offset-encoding=utf-16",
    "--completion-style=detailed"
  }
}
lspconfig.cmake.setup {}
lspconfig.lua_ls.setup {}
lspconfig.pyright.setup {}
lspconfig.vimls.setup {}
lspconfig.ts_ls.setup {}
lspconfig.svelte.setup {}
lspconfig.tailwindcss.setup {}
lspconfig.r_language_server.setup({
  -- Debounce "textDocument/didChange" notifications because they are slowly
  -- processed (seen when going through completion list with `<C-N>`)
  flags = { debounce_text_changes = 150 },
})
lspconfig.marksman.setup {}

-- Configure the dictionary
lspconfig.ltex.setup {
  on_attach = on_attach,
  capabilities = capabilities,
  settings = {
    ltex = {
      dictionary = {
        ['en-US'] = {
          "SvelteKit",
          "Smoljames",
          "Supabase",
          "Emscripten",
          "StackOverflow"
        }
      }
    }
  }
}

if not vim.fn.has('macunix') then
  return
end

-- Swift LSP configuration
--
-- Upon reading the reference page:
-- https://medium.com/swlh/ios-development-on-vscode-27be37293fe1
--
-- Let's dissect the compile command line on a Xcode project using SwiftUI:
-- $ swift-frontend \
--     -c -primary-file $ALL_SWIFT_FILES \
--     $OTHER_COMPILE_OPTIONS \
--     -I$INCLUDE_DIR_PATH_1 ... -I$INCLUDE_DIR_PATH_N \
--     -sdk /Applications/Xcode.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator.sdk \
--     -target arm64-apple-ios16.4-simulator
lspconfig.sourcekit.setup{
  cmd = {
    '/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/sourcekit-lsp',
    -- Append the compile command arguments in the sequel.
    --
    -- IMPORTANT DETAIL:
    -- append -Xswift for **every** single command line arguments.
    --
    -- If your compile command contains the following options:
    -- $ swift-frontend -sdk -/Applications/Xcode.app/.../iPhoneSimulator.sdk
    --
    -- them readapt the compile command options as
    -- -Xswiftc -sdk -Xswiftc -/Applications/Xcode.app/.../iPhoneSimulator.sdk
    --
    -- A good way to understand what is going on is to type on the Shell terminal
    -- by trial-and-error until sourcekit-lsp server runs without stopping
    -- prematurely:
    --
    -- $ sourcekit-lsp \
    --     -Xswiftc -$KEY1 -Xswiftc $VALUE1 \
    --     -Xswiftc -$KEY2 -Xswiftc $VALUE2 \
    --     ...
    --     -Xswiftc -$KEYN -Xswiftc $VALUEN \
    -- Notice the dash key "-" on the compile option "-$KEY"
    '-Xswiftc', '-sdk',
    '-Xswiftc', '/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator17.2.sdk',
    '-Xswiftc', '-target',
    '-Xswiftc', 'arm64-apple-ios13.0-simulator',
    -- -framework AppKit -lz -framework DeviceCheck -framework Security -framework StoreKit -lsqlite3 -lc++ -lz -lsqlite3 -lc++ -lz -framework StoreKit -framework FirebaseAnalytics -framework GoogleAppMeasurementIdentitySupport -framework GoogleAppMeasuremen
  },
  filetypes = { "swift", "objective-c", "objective-cpp" },
  -- root = root_pattern("Package.swift", ".git")
}


vim.api.nvim_create_autocmd('LspAttach', {
  pattern = "swift",
  -- group = vim.api.nvim_create_augroup('UserLspConfig', {}),
  callback = function(ev)
    --enable omnifunc completion
    vim.bo[ev.buf].omnifunc = 'v:lua.vim.lsp.omnifunc'

    -- buffer local mappings
    local opts = { buffer = ev.buf }
    -- go to definition
    vim.keymap.set('n','<Leader>gd',vim.lsp.buf.definition,opts)
    --puts doc header info into a float page
    vim.keymap.set('n','<Leader>K',vim.lsp.buf.hover,opts)

    -- workspace management. Necessary for multi-module projects
    vim.keymap.set('n','<space>wa',vim.lsp.buf.add_workspace_folder, opts)
    vim.keymap.set('n','<space>wr',vim.lsp.buf.remove_workspace_folder, opts)
    vim.keymap.set('n','<space>wl',function()
            print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
    end,opts)

    -- add LSP code actions
    vim.keymap.set({'n','v'},'<space>ca',vim.lsp.buf.code_action,opts)

    -- find references of a type
    vim.keymap.set('n','gr',vim.lsp.buf.references,opts)
  end,
})
