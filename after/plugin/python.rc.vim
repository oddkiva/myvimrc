" => Python IDE.
"
" Indentation.
autocmd FileType python setlocal foldmethod=indent
autocmd FileType python setlocal textwidth=79

" Jedi-vim
"let g:jedi#force_py_version = 3
let g:neocomplcache_enable_at_startup = 1
if !exists('g:neocomplcache_omni_functions')
  let g:neocomplcache_omni_functions = {}
endif
let g:neocomplcache_omni_functions['python'] = 'jedi#completions'
let g:jedi#popup_on_dot = 0

" Vim-flake8
autocmd FileType python map <buffer> <C-b> :call Flake8()<CR>
autocmd FileType python noremap <C-K><C-F> :Autoformat<CR>
autocmd FileType python nnoremap K :grep! "\b<C-R><C-W>\b"<CR>:cw<CR>

" Check Python files with flake8 and pylint.
let g:ale_linters = {'python': ['flake8']}
" " Fix Python files with autopep8 and yapf.
let g:ale_fixers = {'python': ['autopep8', 'yapf']}
" Disable warnings about trailing whitespace for Python files.
let b:ale_warn_about_trailing_whitespace = 0
