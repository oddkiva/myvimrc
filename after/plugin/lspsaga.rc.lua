require("lspsaga").setup {
  server_filetype_map = {
    typescript = 'typescript'
  },
  finder = {
    vsplit = 'v'
  }
}
local opts = { noremap = true, silent = true }
vim.keymap.set('n', '<C-j>', '<Cmd>Lspsaga diagnostic_jump_next<CR>', opts)
vim.keymap.set('n', '<Leader>k', '<Cmd>Lspsaga hover_doc<CR>', opts)
vim.keymap.set('n', '<Leader>f', '<Cmd>Lspsaga finder<CR>', opts)
vim.keymap.set('i', '<C-k>', '<Cmd>Lspsaga peek_definition<CR>', opts)
vim.keymap.set('n', '<Leader>j', '<Cmd>Lspsaga goto_definition<CR>', opts)
vim.keymap.set('n', '<Leader>r', '<Cmd>Lspsaga rename<CR>', opts)
vim.keymap.set('n', '<Leader>so', '<Cmd>Lspsaga outline<CR>', opts)
