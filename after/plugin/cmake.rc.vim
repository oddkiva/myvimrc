" => CMake IDE
function! CmakeFormat()
  silent !clear
  execute "!" . g:cmake_format . " " . bufname("%") . " -i"
endfunction

if !exists("g:cmake_format")
  let g:cmake_format = "cmake-format"
  autocmd FileType cmake nnoremap <buffer> <C-K><C-F> :call CmakeFormat()<cr>
endif
