require("bufferline").setup {
  options = {
    separator_style = "slant", -- | "slope" | "thick" | "thin" | { 'any', 'any' },
    hover = {
      enabled = true,
      delay = 0,
      reveal = {'close'}
    }
  }
}
