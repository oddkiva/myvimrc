require("nvim-treesitter.configs").setup {
  highlight = {
    enable = true,
    disable = {},
  },
  indent = {
    enable = true,
    disable = {},
  },
  ensure_installed = {
    "javascript",
    "lua",
    "markdown",
    "markdown_inline",
    "svelte",
    "typescript",
  },
}
