-- cf. Documentation https://github.com/ray-x/lsp_signature.nvim
local lsp_signature = require("lsp_signature")

lsp_signature.setup({
  bind = true,
  handler_opts = {
    border = "rounded"
  }
})

