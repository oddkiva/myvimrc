" ==============================================================================
" Setup plugins

" First do this, specifically for nvim-tree.lua.
if has('macunix')
  let g:python3_host_prog = '/opt/homebrew/bin/python3.11'
elseif has('unix')
  let g:python3_host_prog = '/usr/bin/python3'
endif
let g:loaded_netrw       = 1
let g:loaded_netrwPlugin = 1

filetype off

if has("unix")
  call plug#begin('~/.config/nvim/plugged')
else
  call plug#begin('~/vimfiles/plugged')
endif

" call plug#begin()
  " ========================================================================== "
  " Default set of vim settings that everyone can agree on.
  Plug 'tpope/vim-sensible'

  " Asynchronous command support.
  Plug 'tpope/vim-dispatch'

  " Easily switch buffer and other things.
  Plug 'tpope/vim-unimpaired'
  " Even better...
  Plug 'vim-ctrlspace/vim-ctrlspace'

  " Multi-colored cursor.
  " Plug 'mg979/vim-visual-multi', {'branch': 'master'}

  " Conversion from camelcase to snake case etc.
  Plug 'tpope/vim-abolish'  " keybindings: crc, crs

  " Keybindings helper.
  Plug 'folke/which-key.nvim'

  " Remote development infrastructure.
  Plug 'chipsenkbeil/distant.nvim', { 'branch': 'v0.3' }

  " ========================================================================== "
  " Vim theme.
  "
  " Base 16 colorschemes
  Plug 'chriskempson/base16-vim'
  " Display file icons for the NeoVim plugins.
  Plug 'nvim-tree/nvim-web-devicons'

  " ========================================================================== "
  " Additional GUI and behavioral features.
  "
  " Zoom in/out text.
  Plug 'drmikehenry/vim-fontsize'
  " Tree navigation for file browsing.
  Plug 'nvim-tree/nvim-tree.lua'
  " Full path fuzz file, buffer, mru, tag, ... finder.
  Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
  Plug 'junegunn/fzf.vim'

  " Tab bar on NeoVim.
  "
  " Bufferline is more ergonomic than barbar.
  "
  Plug 'akinsho/bufferline.nvim', { 'tag': '*' }

  " For better looking ViM status.
  Plug 'vim-airline/vim-airline'
  Plug 'vim-airline/vim-airline-themes'

  " For quick string search
  Plug 'rking/ag.vim'
  " Enhanced tab.
  Plug 'ervandew/supertab'
  " Simple motions in vim.
  Plug 'easymotion/vim-easymotion'
  " Integration with Git.
  Plug 'tpope/vim-fugitive'

  " To automatically enclose code within parentheses, quotes, or whatever.
  Plug 'tpope/vim-surround'  " help: surround

  " To highlight indent levels in ViM.
  Plug 'nathanaelkane/vim-indent-guides'  " help: indent-guides, (<Leader>ig.)

  " ========================================================================== "
  " LSP
  Plug 'williamboman/mason.nvim'
  Plug 'williamboman/mason-lspconfig.nvim'
  Plug 'neovim/nvim-lspconfig'
  Plug 'ray-x/lsp_signature.nvim'
  " main one
  Plug 'ms-jpq/coq_nvim', {'branch': 'coq' }
  " 9000+ Snippets
  Plug 'ms-jpq/coq.artifacts', {'branch': 'artifacts'}
  " lua & third party sources -- See https://github.com/ms-jpq/coq.thirdparty
  " Need to **configure separately**
  Plug 'ms-jpq/coq.thirdparty', {'branch': '3p'}
  " - shell repl
  " - nvim lua api
  " - scientific calculator
  " - comment banner
  " - etc

  " Syntax highlighting.
  Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

  " Documentation retrieval utilities.
  Plug 'glepnir/lspsaga.nvim'

  " Multi-language code autoformatting
  Plug 'Chiel92/vim-autoformat'

  " Fast HMTL editing.
  Plug 'mattn/emmet-vim'

  " HTML/JS/svelte syntax highlighting and code formatting.
  Plug 'prettier/vim-prettier', { 'do': 'npm install', 'for': ['html', 'javascript', 'svelte', 'typescript']  }

  " GLSL support.
  Plug 'tikhomirov/vim-glsl'

  " Python support.
  Plug 'davidhalter/jedi-vim', { 'for': 'python' }
  Plug 'hynek/vim-python-pep8-indent', { 'for': 'python' }
  Plug 'jmcantrell/vim-virtualenv', { 'for': 'python' }

  " Shell script support.
  Plug 'vim-scripts/sh.vim--Cla'

  " Powershell script support.
  Plug 'PProvost/vim-ps1'

  " Protobuf
  Plug 'jdevera/vim-protobuf-syntax'

  " Swift
  Plug 'tokorom/vim-swift-format'

  " QML
  Plug 'peterhoeg/vim-qml'

  " Latex support
  Plug 'lervag/vimtex'
  Plug 'KeitaNakamura/tex-conceal.vim', { 'for': 'tex' }
  Plug 'xuhdev/vim-latex-live-preview'

  " C++ IDE.
  if has("unix")
    Plug 'octol/vim-cpp-enhanced-highlight', { 'for': 'cpp' }
  endif
call plug#end()


let g:CtrlSpaceDefaultMappingKey = "<C-f>"

" ==============================================================================
" => GUI behavioral options.
"
" No annoying sound on errors.
set noerrorbells
set novisualbell
set t_vb=
set tm=500

" Allow arrow keys to navigate in text.
set whichwrap+=<,>,h,l

" Enable mouse.
set mouse=a
set mousemoveevent


" ==============================================================================
" => GUI general display options.
lua << EOF
require("ui-behavior")
require("ui-look")
require("file-extensions")

-- Start COQ autocompletion engine.
vim.g.coq_settings = {
  auto_start = true,
}

EOF

" Set extra options when running in GUI mode
if has("gui_running")
  set guioptions-=m
  set guioptions-=r
  set guioptions-=L
  set guioptions-=T
  set guioptions+=e
  set guitablabel=%M\ %t

  if has("gui_gtk2") || has("gui_gtk3")
    set guifont=JetBrains\ Mono\ Medium:h9
  elseif has("gui_macvim")
    set guifont=JetBrainsMono\ Nerd\ Medium:h12
  elseif has("gui_win32")
    set guifont=Consolas:h11:cANSI
  endif
endif


" vim-ctrlspace clashes with bufferline.
let g:CtrlSpaceUseTabline = 0


" ==============================================================================
" => Specify the behavior when switching between buffers
try
  set switchbuf=useopen,usetab,newtab
  set stal=2
catch
endtry

" Return to last edit position when opening files (You want this!)
autocmd BufReadPost *
      \ if line("'\"") > 0 && line("'\"") <= line("$") |
      \   exe "normal! g`\"" |
      \ endif

" Remember info about open buffers on close
set viminfo^=%
