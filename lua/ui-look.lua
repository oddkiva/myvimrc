-- Needed for base16 colorschemes.
vim.cmd [[colorscheme base16-ocean]]

-- Vim-airline
-- vim.g["airline_theme"] = 'bubblegum'
vim.g["airline#extensions#tabline#enabled"] = 0
vim.g["airline_powerline_fonts"] = 1
vim.g["airline#extensions#ale#enabled"] = 1

-- Buffer separator.
vim.cmd [[ set fillchars+=vert:\▏ ]] -- Notice the whitespace 
vim.cmd [[ hi vertsplit guibg=bg guifg=fg ]]
