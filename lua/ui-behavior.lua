-- Always show line number of the cursor.
vim.opt.number = true
-- A buffer becomes hidden when it is abandoned
vim.opt.hidden = true

-- For more evolved look and feel.
vim.opt.termguicolors = true

-- Ignore case when searching.
vim.opt.ignorecase = true
-- When searching try to be smart about cases.
vim.opt.smartcase = true
-- Highlight search results.
vim.opt.hlsearch = true
-- For regular expressions turn magic on.
vim.opt.magic = true


-- => Files, backups and undo
--
-- Turn backup off, since most stuff is in SVN, git etc anyway...
vim.opt.backup = false
vim.opt.swapfile = false

-- => Edit options.
--
-- Set utf8 as standard encoding and en_US as the standard language
vim.opt.encoding = "utf-8"
-- " Use Unix as the standard file type
vim.opt.ffs = "unix"
-- Use spaces instead of tabs
vim.opt.expandtab = true
-- 2 spaces per tabs.
vim.opt.shiftwidth = 2
vim.opt.tabstop = 2
-- Linebreak on 80 characters
vim.opt.linebreak = true
vim.opt.textwidth = 80
-- Smart indent
vim.opt.smartindent = true
-- Wrap lines
vim.opt.wrap = true

-- => Code folding options.
--
vim.g["sh_fold_enabled"] = 7
vim.g["is_bash"] = 1
vim.opt.foldenable = false
vim.opt.foldmethod= "indent"
