# The master configuration files.
ln -sf $(pwd)/_vimrc ~/.config/nvim/init.vim

# The autoload module folders.
ln -sf $(pwd)/autoload ~/.config/nvim/autoload
ln -sf $(pwd)/after ~/.config/nvim/after
ln -sf $(pwd)/lua ~/.config/nvim/lua
